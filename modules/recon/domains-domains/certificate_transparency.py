from recon.core.module import BaseModule
import json

class Module(BaseModule):

    meta = {
        'name': 'Certificiate Transparency Search',
        'author': 'Rich Warren (richard.warren@nccgroup.trust)',
        'description': 'Searches certificate transparency data from crt.sh, adding newly identified hosts to the hosts table.',
        'query':'SUPPLY DOMAIN',
        'options': (
            (
                ('timeout', True, 60, 'HTTP request timeout')
            ),
            (
                ('omit_subdomains', True, 60, 'Disregard subdomains, e.g. subdomain.domain.tld')
            ),
        ),
    }

    def module_run(self, target):
        for target in target:
            self.heading(target, level=0)
            resp = self.request('https://crt.sh/?q={0}&output=json'.format(target),timeout=60)

            if resp.status_code != 200:
                self.output('Invalid response for \'%s\'' % domain)
                continue

            # Iterate over each json response
             # skip wlidcard certificates
             # add the new host value to the date
            for cert in json.loads(resp.raw):

                # get the name value
                nv = cert.get('name_value')

                # Skip wildcard certs
                if self.options.get('omit_subdomains') and nv.split('.').__len__() > 2:
                    continue

                # add the new host
                  # add_hosts will assure uniqueness of the host name
                self.add_domains(nv)
