from recon.core.module import BaseModule
import json

class Module(BaseModule):

    meta = {
        'name': 'Certificiate Transparency Search',
        'author': 'Rich Warren (richard.warren@nccgroup.trust)',
        'description': 'Searches certificate transparency data from crt.sh, adding newly identified hosts to the hosts table.',
        'query': 'SELECT DISTINCT domain FROM domains WHERE domain IS NOT NULL',
        'options': (
            (
                ('skip_wildcards', True, True, 'Skip wildcard certificates')
            ),
            (
                ('timeout', 60, True, 'HTTP request timeout')
            ),
        ),
    }

    def module_run(self, domains):
        for domain in domains:
            self.heading(domain, level=0)
            resp = self.request('https://crt.sh/?q=%25.{0}&output=json'.format(domain),
                timeout=self.options.get('timeout'))
            if resp.status_code != 200:
                self.output('Invalid response for \'%s\'' % domain)
                continue

            # Iterate over each json response
             # skip wlidcard certificates
             # add the new host value to the date
            for cert in json.loads(resp.raw):

                # get the name value
                nv = cert.get('name_value')

                # Skip wildcard certs
                if self.options.get('skip_wildcards') and nv[0] == '*':
                    continue

                # add the new host
                  # add_hosts will assure uniqueness of the host name
                self.add_hosts(nv)
